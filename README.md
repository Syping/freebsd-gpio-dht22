# DHT22 reading on FreeBSD

Modified DHT11 code for DHT22, JSON OUTPUT

Uses [libgpio](https://bitbucket.org/rpaulo/libgpio/src) by Rui Paulo.

### Using
Change your pin, compile with:
```
$ clang main.c libgpio.c -o dht
$ ./dht
```
